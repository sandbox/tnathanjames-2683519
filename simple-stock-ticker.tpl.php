 <?php
/**
 * @file
 * The default template file for the Simple Stock Ticker.
 *
 * Avaliable variables:
 * - $quote: An array containing placeholders that will be updated on an
 *   interval if printed.
 * - $quote_instance: The first instance pulled from Yahoo finance.  Printing
 *   these will not result in updated content.
 *
 * For both of the arrays above, the members are discovered by the returned
 * keys from the yahoo finance feed.  The current list names are listed
 * below.
 * - Current members of the $quote and $quote_instance arrays include the following:
 *   - change
 *   - chg_percent
 *   - day_high
 *   - day_low
 *   - issuer_name
 *   - issuer_name_lang
 *   - name
 *   - price
 *   - symbol
 *   - ts
 *   - type
 *   - utctime
 *   - volume
 *   - year_high
 *   - year_low
 */
?>
<div class="simple-stock-ticker">
  <h2><?php print $quote_instance['symbol']; ?></h2>
  <?php print render($quote['price']); ?>
  <?php print render($quote['change']); ?>
  <?php print render($quote['chg_percent']); ?>
</div>
