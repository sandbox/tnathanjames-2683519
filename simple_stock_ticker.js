/**
 * @file
 * Insert and update data for the simple stock ticker.
 */

(function ($) {
  'use strict';

  /**
   * Insert and update Yahoo stock quote information in the simple stock ticker.
   */
  Drupal.behaviors.simpleStockTicker = {
    attach: function (context, settings) {
      var symbol;
      if (context === document) {
        if (typeof settings.simpleStockTicker !== 'undefined') {
          if (typeof settings.simpleStockTicker.symbol !== 'undefined') {
            symbol = settings.simpleStockTicker.symbol;
          }
        }
        var quoteUrl = '/simple_stock_ticker/get/ajax/' + symbol;
        var updateQuote = function updateQuote() {
          $.ajax({
            url: quoteUrl,
            dataType: 'json',
            success: function (data) {
              $('.simple-stock-ticker-field', context).each(function (index) {
                var $self = $(this);
                var fieldName = $self.data('field');
                var value = data.list.resources[0].resource.fields[fieldName];
                if (value !== 'undefined' && value !== 'null') {
                  var addSymbol = '';
                  var addSymbolAfter = '';
                  var forcePlus = '';
                  if ($self.data('addsymbol')) {
                    addSymbol = $self.data('addsymbol');
                  }
                  if ($self.data('addsymbolafter')) {
                    addSymbolAfter = $self.data('addsymbolafter');
                  }
                  if (parseFloat(value) && parseFloat(value) < 0) {
                    $self.removeClass('positive').addClass('negative');
                  }
                  else {
                    $self.removeClass('negative').addClass('positive');
                    if ($self.data('forceplus')) {
                      forcePlus = '+';
                    }
                  }
                  if ($self.data('roundto')) {
                    value = Number(value).toFixed($self.data('roundto'));
                  }
                  $self.text(forcePlus + addSymbol + value + addSymbolAfter);
                }
              });
            }
          });
        };

        if (symbol !== 'undefined') {
          updateQuote();
          setInterval(updateQuote, settings.simpleStockTicker.refreshInterval);
        }
      }
    }
  };

})(jQuery);