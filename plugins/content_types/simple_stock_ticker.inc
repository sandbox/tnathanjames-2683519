<?php

/**
 * @file
 * The plugin definition and implementation of the simple stock ticker.
 */

/**
 * The plugin definition.
 *
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Simple Stock Ticker'),
  'description' => t('A simple stock ticker that retrieves and displays information about one symbol'),
  'single' => TRUE,
  'category' => t('Miscellaneous'),
  'edit form' => 'simple_stock_ticker_settings_form',
  'render callback' => 'simple_stock_ticker_render',
  'admin info' => 'simple_stock_ticker_admin_info',
  'defaults' => array(
    'symbol' => '',
    'refresh' => '300',
  ),
);

/**
 * Ctools plugin settings form for the simple stock ticker pane.
 */
function simple_stock_ticker_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['symbol'] = array(
    '#title' => t('Symbol'),
    '#description' => t('Enter the stock symbol to show.'),
    '#type' => 'textfield',
    '#default_value' => $conf['symbol'],
    '#required' => TRUE,
  );

  $form['refresh'] = array(
    '#title' => t('Refresh time'),
    '#description' => t('Enter how often the Stock Ticker should refresh in Seconds.'),
    '#type' => 'textfield',
    '#default_value' => $conf['refresh'],
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Ctools plugin settings form submit function.
 */
function simple_stock_ticker_settings_form_submit(&$form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Ctools plugin render callback function.
 */
function simple_stock_ticker_render($subtype, $conf, $panel_args, $context = NULL) {
  $block = new stdClass();

  $block->title = $conf['symbol'];

  // Setup markup placeholders that will be updated by stock info by the attached js.
  $block->content = array(
    '#markup' => theme('simple_stock_ticker', array('symbol' => $conf['symbol'])),
    '#attached' => array(
      'js' => array(
        array(
          'data' => drupal_get_path('module', 'simple_stock_ticker') . '/simple_stock_ticker.js',
          'type' => 'file',
        ),
        array(
          'data' => array(
            'simpleStockTicker' => array(
              'symbol' => $conf['symbol'],
              'refreshInterval' => intval($conf['refresh']) * 1000,
            ),
          ),
          'type' => 'setting',
        ),
      ),
    ),
  );
  return $block;
}

/**
 * Ctools plugin admin info function.
 *
 * This displays the block title and the symbol in the panels interface.
 */
function simple_stock_ticker_admin_info($subtype, $conf, $context) {
  if (!empty($conf)) {
    $block = new stdClass;
    $block->title = $conf['override_title'] ? $conf['override_title_text'] : '';
    $block->content = t('Showing Stock Ticker for @symbol', array(
      '@symbol' => $conf['symbol'],
     ));
     return $block;
  }
}
